<div>
    <h1 class="title">List of books</h1>
    <table class="table">
        <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>ISBN</td>
                <td>Publication date</td>
                <td>Authors</td>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach($GLOBALS['DATA']['BOOKS'] as $book) {
                $authors = [];
                if (is_array($book->authors)) {
                    foreach ($book->authors as $author) {
                        $authors[] = '<span class="tag is-success">'.$author->lastName.' '.$author->firstName.'</span>';
                    }
                } else if (is_object($book->authors)) {
                  $authors[] =   '<span class="tag is-success">'.$book->authors->lastName.' '.$book->authors->firstName.'</span>';
                }
//                print('<pre>'.print_r($book, true).'</pre>');
                echo '<tr>'.
                    '<td>'.$book->id.'</td>'.
                    '<td>'.$book->title.'</td>'.
                    '<td>'.$book->ISBN.'</td>'.
                    '<td>'.$book->publicationDate.'</td>'.
                    '<td>'.implode(" ", $authors).'</td>'
                    .'</tr>';
            }
        ?>
        </tbody>
    </table>
</div>