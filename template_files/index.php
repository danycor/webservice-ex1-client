<!DOCTYPE html>
<html lang="fr">
<head>
    <title>SOAP Client PHP</title>
    <link rel="stylesheet" href="bulma.min.css">
</head>

<body style="display: flex">
<div style="max-width: 200px; max-height: 100vh;">
<?php include 'html/nav.html';?>
</div>
<div>
    <?php if($GLOBALS['CONTENT_PHP'] !== "") {require($GLOBALS['CONTENT_PHP']);} ?>
</div>

</body>
</html>