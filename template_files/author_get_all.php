<div>
    <h1 class="title">List of authors</h1>
    <table class="table">
        <thead>
            <tr>
                <td>ID</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Books</td>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach($GLOBALS['DATA']['AUTHORS'] as $author) {
                $books = [];
                if (is_array($author->books)) {
                    foreach ($author->books as $book) {
                        $books[] = '<span class="tag is-success">'.$book->title.'</span>';
                    }
                } else if (is_object($author->books)) {
                    $books[] =   '<span class="tag is-success">'.$author->books->title.'</span>';
                }
                echo '<tr>'.
                    '<td>'.$author->id.'</td>'.
                    '<td>'.$author->firstName.'</td>'.
                    '<td>'.$author->lastName.'</td>'.
                    '<td>'.implode(" ", $books).'</td>'
                    .'</tr>';
            }
        ?>
        </tbody>
    </table>
</div>