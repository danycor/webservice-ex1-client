<?php


class DefaultController
{
    public function run($method) {
        if(method_exists($this, $method)) {
            $this->$method();
        } else {
            $this->index();
        }
    }

    public function index() {
        include './template_files/index.php';
    }
}