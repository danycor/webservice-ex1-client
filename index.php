<?php
include "DefaultController.php";
include "BookController.php";
include "AuthorController.php";

$class = isset($_GET['class']) ? $_GET['class'] : "DefaultController";
$method = isset($_GET['method']) ? $_GET['method'] : "index";
$GLOBALS['CONTENT_PHP'] = "";
$GLOBALS['DATA'] = [];

if(class_exists($class)) {
    $controller = new $class();
    if($controller instanceof DefaultController) {
        $controller->run($method);
    }
} else {
    echo $class . " doesent exist";
}

//$url = "http://localhost:8081/authorImp?wsdl";
//$client = new SoapClient($url);
//
//$authors = $client->__soapCall("getAllAuthors", []);
////echo "test ".count($authors);
//foreach ($authors->return as $author) {
//    echo "Author : ". $author->firstName." ". $author->lastName."\n";
//}
?>
