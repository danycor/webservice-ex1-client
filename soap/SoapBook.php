<?php


class SoapBook
{
    protected $url = "http://localhost:8081/bookImp?wsdl";


    public function getAll() {
        $client = new SoapClient($this->url);
        $response = $client->__soapCall("getAllBooks", []);
        return $response->return;
    }

}