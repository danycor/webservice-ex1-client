<?php


class SoapAuthor
{
    protected $url = "http://localhost:8081/authorImp?wsdl";


    public function getAll() {
        $client = new SoapClient($this->url);
        $response = $client->__soapCall("getAllAuthors", []);
        return $response->return;
    }

}