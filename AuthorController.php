<?php
include 'soap/SoapAuthor.php';

class AuthorController extends DefaultController
{
    public function getAll() {
        $soapClient = new SoapAuthor();
        $GLOBALS['CONTENT_PHP'] = "author_get_all.php";
        $GLOBALS['DATA']['AUTHORS'] = $soapClient->getAll();
//        print_r($GLOBALS['DATA']['BOOKS']);
        include 'template_files/index.php';
    }

}