<?php
include 'soap/SoapBook.php';

class BookController extends DefaultController
{
    public function getAll() {
        $soapClient = new SoapBook();
        $GLOBALS['CONTENT_PHP'] = "book_get_all.php";
        $GLOBALS['DATA']['BOOKS'] = $soapClient->getAll();
//        print_r($GLOBALS['DATA']['BOOKS']);
        include 'template_files/index.php';
    }

}